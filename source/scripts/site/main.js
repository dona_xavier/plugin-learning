$(window).on('load', function () {
    $('body').addClass('is-loaded'); 
}); 
$(document).ready(function(){
    $('.slider__wrapper').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 4,
        responsive: [
            {
              breakpoint: 1199,
              settings: {
                slidesToShow: 3,
                centerMode: true,
                centerPadding: '0px',
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
    });
  });
  filterSelection("new")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("tab__list");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);     
    }
  }
  element.className = arr1.join(" ");
}


// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("tab__buttons");
var btns = btnContainer.getElementsByClassName("tab__button");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function(){
    var current = document.getElementsByClassName("tab__button-active");
    current[0].className = current[0].className.replace(" tab__button-active", "");
    this.className += " tab__button-active";
  });
}